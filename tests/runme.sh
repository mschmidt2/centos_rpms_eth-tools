#!/bin/bash
set -x

# This is a workaround for eth-tools OSCI gating test. Until OSCI
# support submit test to designated beaker machines, which have the
# hardware for eth-tools, we can't run OSCI test for eth-tools.

exit 0
